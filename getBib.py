#!/usr/bin/env python

from sys import version_info

assert version_info >= (3, 3)

from urllib.parse import quote
from sys import argv
import requests
import json
import re

def query(search,nResults=10):
    DOIs = []

    try:
        req = requests.get("https://api.crossref.org/works?query="+quote(search), allow_redirects = True)
        req.raise_for_status()
        outcome = req.json()
    except:
        print("Error with the request.")
        return 1

    if outcome['message']['total-results'] >= nResults:
        outNum = nResults
    else:
        outNum = outcome['message']['total-results']

    stResult = 0
    
    #print(outNum)
    while True:
        for i in range(stResult,outNum):
            title = ""
            year = 0000
            authors = ""
            try:
                title = outcome['message']['items'][i]['title'][0]
                year = outcome['message']['items'][i]['created']['date-parts'][0][0]
                authors = ""

                for author in outcome['message']['items'][i]['author']:
                    authors += ", " + author['family']

                doiUrl = str(outcome['message']['items'][i]['URL'])
                DOIs.append(doiUrl)
            except:
                DOIs.append("")
                #print("Author error")

            #print(label.replace("&na",""))
            print("%2d %s (%i)%s\n------"%(i,title,year,authors))

        select = input("Showing results from %i to %i of %i. Enter n(p) for the next(previous) %i results. What is the correct article? "%(stResult, outNum,outcome['message']['total-results'],nResults))
        if select.isdigit():
            return DOIs[int(select)].replace("http://dx.doi.org/","")
        elif select == "n":
            tmpN = outNum + nResults
            if tmpN < 20:
                outNum = tmpN
                stResult += nResults
        elif select == "p":
            tmpN = stResult - nResults
            if tmpN >= 0:
                stResult = tmpN
                outNum -= nResults
        else:
            return "NA"

def getBibItem(idDoi):
    base_url = "http://dx.doi.org/"
    head = dict({'User-Agent': 'python-requests/'+requests.__version__, 'X-USER-AGENT': 'python-requests/'+requests.__version__,'Accept': 'application/x-bibtex' })
    req = requests.get(base_url+idDoi, headers = head, allow_redirects = True)
    req.raise_for_status()
    req.encoding = "UTF-8"
    return req.text

def append2File(outFile,string):
    with open(outFile, "a") as output:
        output.write(string)
        output.write("\n\n")

def pHelp():
    print("Usage: getBib.py [BIBFILE] [OPTION] [INPUTS]\n\nOPTIONS\n\t-aq [SEARCH]\tget bib metadata from a search.\n\t-ai [DOI]\tget bib metadata from doi id.\n\t-h [DOI]\tshow this.\n")

if len(argv) > 1:
    outBib = "outBib.bib"
    for i in range(1,len(argv)):
        if argv[i].find(".bib") != -1:
            outBib = argv[i]
        elif argv[i] == "-aq":
            if len(argv) < i+1:
                print("Give the query after the -aq separated by a space.")
                quit()
            else:
                qText = ""
                for j in range(i+1, len(argv)):
                    qText += " "+argv[j]

                print("Searching for: "+qText+"...\n")
                idDoi = query(qText)
                if re.match('10.[0-9]{4,}/[0-9a-zA-Z]',idDoi):
                    print("Getting bibtex metadata and appending it to %s."%(outBib))
                    bibText = getBibItem(idDoi)
                    append2File(outBib, bibText)
                    quit()

        elif argv[i] == "-ai":
            if len(argv) < i+1:
                print("Give the doi after the -ai separated by a space.")
                quit()
            else:
                DOI = re.sub(".+doi.org/", "", argv[i+1])
                if re.match('10.[0-9]{4,}/[0-9a-zA-Z]',DOI):
                    bibText = getBibItem(DOI)
                    append2File(outBib, bibText)
                    quit()
                else:
                    print("Your input does not look like a DOI.")
                    quit()
        else:
            pHelp()
            break
else:
    pHelp()


