#!/usr/bin/python3

from sys import argv
from sys import version_info
from os.path import isfile

assert version_info >= (3, 3)

import re

# this function applyes my pattern for citation labels
# Year + 1st author in lower case + 1st letter of up to 3 other authors in upper case
def fixLabels(bibFile):
    wholeBib = dict()
    with open(bibFile,"r") as inBib:
        text = inBib.readlines()
        headers = list()
        for i in range(len(text)):
            if text[i].find("@") != -1:
                headers.append(i)
        for i in range(len(headers)):
            j = headers[i]
            bibType = text[j].split("{")[0]
            bibInfo = str()
            while text[j] != "}\n":
                j += 1 
                bibInfo += text[j]
                if text[j].find("author") != -1:
                    authors = text[j].split("= {")[1].replace("},\n","").split(" and ")
                    stAuth = re.sub("[^a-zA-Z]+", "",authors[0].split(" ")[-1]).lower()
                    ndAuth = str()
                    for others in authors[1:]:
                        ndAuth += re.sub("[^a-zA-Z]+", "",others.split(" ")[-1])[0]
                    if len(ndAuth) > 3:
                        ndAuth = ndAuth[:3]
                    #print(ndAuth.upper())
                if text[j].find("year") != -1:
                    year =  int(re.sub("[^0-9]+","",text[j].split("=")[1]))
            tag = "%i%s%s"%(year, stAuth, ndAuth.upper())
            bibInfo = bibType+"{"+tag+",\n"+bibInfo
            wholeBib[tag] = bibInfo
        #with open(outBib,"w") as outBib:
        with open(bibFile,"w") as outBib:
            #writes the file ordering by the citation labels
            for key in sorted(wholeBib):
                outBib.write(wholeBib[key]+"\n")

#This function just reorders the bib file according to the citation labels
def orderBib(bibFile):
    wholeBib = dict()
    with open(bibFile,"r") as inBib:
        text = inBib.readlines()
        headers = list()
        for i in range(len(text)):
            if text[i].find("@") != -1:
                headers.append(i)
        for i in range(len(headers)):
            j = headers[i]
            tag = text[j].split("{")[1].split(",")[0]
            bibInfo = str()
            bibInfo += text[j]
            j += 1 
            while text[j] != "}\n" or text[j] != "\n" or text[j].find("@") == -1 :
                bibInfo += text[j]
                j += 1 
            wholeBib[tag] = bibInfo
        #with open(outBib,"w") as outBib:
        KEYS = sorted(wholeBib)
        with open(bibFile,"w") as outBib:
            for key in KEYS:
                outBib.write(wholeBib[key]+"\n")

def pHelp():
    print("Usage: bibmanager.py [BIBFILE] [OPTION]\n\nOPTIONS\n\t-st\tsort bibtex file according to labels.\n\t-rl\trelabel bibtex metadata and sort it.\n")

if len(argv) > 1:
    outBib = ""
    sort = False
    relabel = False
    for i in range(1,len(argv)):
        if argv[i].find(".bib") != -1:
            outBib = argv[i]
        elif argv[i] == "-st":
            sort = True
        elif argv[i] == "-rl":
            relabel = True
        elif argv[i] == "-h":
            pHelp()
            quit()
    
    if isfile(outBib):
        if relabel:
            fixLabels(outBib)
        elif sort:
            orderBib(outBib)
    else:
        print(outBib+" is not a file.")

else:
    pHelp()


