# Simple bibtex manager

The aim of this project is to facilitate the usage of bibtex by both get bibtex data and organizing the library.

## Prerequisites

This script only requires python3.3 or higher and an internet connection.

## getBib.py

This script gets bibtex metadata from [dx.doi.org](http://www.dx.doi.org) via API. 

### Usage

It has two basic usages

* Get bibtex data from doi and append to `bibFile.bib`. That can be used as:

```
getBib.py bibFile.bib -ai 10.0000/doiOfTheReference
```

* Search article in [crossref.org](http://www.crossref.org) and then get the doi value and do the same as the previous option. That can be used as

```
getBib.py bibFile.bib -aq All of this this will be searched for
```

All the input given after the `-aq` tag will be considered part of your query. In both cases if no `.bib` file is given, the script will automatically append the bibtex data to ```outBib.bib```.

## bibmanager.py

This script organazes the `.bib` file.

### Usage

It has two basic usages

* Sort the bibtex entries by label name

```
getBib.py bibFile.bib -st
```

* Relabel all the entries according to Year + 1st author in lower case + 1st letter of up to 3 other authors in upper case.

```
getBib.py bibFile.bib -rl
```

**CAUTION** this script will rewrite the files and it suppouses that the formating is the one given by `getBib.py`.
